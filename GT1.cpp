#include <stdio.h>

namespace choices {
 enum Option {
    Option_Invalid,
    Option1,
    Option2,
    //others...
};
}



choices::Option resolveOption(std::string input) {
    if( input == "damage head" ) return choices::Option1;
    if( input == "damage chest" ) return choices::Option2;
    if( input == "damage left arm" ) return choices::Option3;
    if( input == "damage right arm" ) return choices::Option4;
    if( input == "damage left leg" ) return choices::Option5;
    if( input == "damage right leg" ) return choices::Option6;
    
    if( input == "Break head" ) return choices::Option7;
    if( input == "Break chest" ) return choices::Option8;
    if( input == "Break left arm" ) return choices::Option9;
    if( input == "Break right arm" ) return choices::Option10;
    if( input == "Break left leg" ) return choices::Option11;
    if( input == "Break right leg" ) return choices::Option12;
    return choices::Option::Option_Invalid;
}

int main() {
	int Pbody[6] = {5,5,5,5,5,5};

	char Pbstats[6][6] = { //first is rows, second is columns (from my understanding)
	"fine",
	"fine",
	"fine",
	"fine",
	"fine",
	"fine",
	};
	
printf("\033[1;31m▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀");
printf("\033[0;0m\nHead: %d (%s) \n", Pbody[0], Pbstats[0]);
printf("Body: %d (%s) \n", Pbody[1], Pbstats[1]);
printf("Left Arm: %d (%s) \n", Pbody[2], Pbstats[2]);
printf("Right Arm: %d (%s) \n", Pbody[3], Pbstats[3]);
printf("Left Leg: %d (%s) \n", Pbody[4], Pbstats[4]);
printf("Right Leg: %d (%s) \n", Pbody[5], Pbstats[5]);
printf("\033[1;31m▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀\n");
printf("\033[0;0m");



}
