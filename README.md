~GT1.C~

**damage** - remove 1 hitpoint from part of body, simply for testing purposes. in future versions, enemies will be the ones damaging your body.
- damage head
- damage chest
- damage left arm
- damage right arm
- damage left leg
- damage right leg
if a body part drops to 0, it breaks. if your head or chest reaches zero, you are dead. 

~readwrite.c~

**add potion** - adds a potion to the inventory, located at inventory.txt
**clear inventory** - clears all items from inventory

once readwrite is complete, it will be integrated into GT1 as a part of the combat simulator. 